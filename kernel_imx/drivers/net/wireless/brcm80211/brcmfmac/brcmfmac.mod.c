#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xd818474, "module_layout" },
	{ 0x88dcdb32, "sdio_writeb" },
	{ 0x35827261, "sdio_readb" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x1ef2af17, "wiphy_free" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0xb811abea, "debugfs_create_dir" },
	{ 0xa4fb175a, "debugfs_create_u8" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0x1dc70f80, "dev_set_drvdata" },
	{ 0x9c64fbd, "ieee80211_frequency_to_channel" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xe949d9c8, "send_sig" },
	{ 0x9c0ba064, "malloc_sizes" },
	{ 0x718565d5, "remove_wait_queue" },
	{ 0x2d3affe0, "sdio_enable_func" },
	{ 0xc7a4fbed, "rtnl_lock" },
	{ 0xab923737, "sdio_claim_irq" },
	{ 0x74008489, "brcmu_pkt_buf_get_skb" },
	{ 0x8bd94317, "_raw_spin_lock_bh" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x8949858b, "schedule_work" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0x4205ad24, "cancel_work_sync" },
	{ 0x4588bf0f, "__dynamic_pr_debug" },
	{ 0xe2fae716, "kmemdup" },
	{ 0x7513e94e, "ieee80211_channel_to_frequency" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x86cb7b28, "init_timer_key" },
	{ 0x88d8ee13, "brcmu_pktq_pdeq_tail" },
	{ 0x881fd158, "mutex_unlock" },
	{ 0x85df9b6c, "strsep" },
	{ 0x6aead09b, "brcmu_pktq_penq" },
	{ 0xdf9e29a1, "brcmu_pktq_mdeq" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x97dc76ca, "skb_realloc_headroom" },
	{ 0x25674d39, "debugfs_remove_recursive" },
	{ 0xc499ae1e, "kstrdup" },
	{ 0x4842d6e1, "kthread_create_on_node" },
	{ 0x7d11c268, "jiffies" },
	{ 0x2c261c15, "sdio_get_host_pm_caps" },
	{ 0x3922c8d, "skb_unlink" },
	{ 0xe2d5255a, "strcmp" },
	{ 0xa5f07583, "netif_rx" },
	{ 0x41e92619, "__init_waitqueue_head" },
	{ 0xffd5a395, "default_wake_function" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xa2ba6066, "sdio_writel" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xf7b574c2, "del_timer_sync" },
	{ 0x5f754e5a, "memset" },
	{ 0xf8c4632, "netif_rx_ni" },
	{ 0x6da6cea1, "__ieee80211_get_channel" },
	{ 0x74c97f9c, "_raw_spin_unlock_irqrestore" },
	{ 0x43da3c35, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0xd3ec1f87, "kthread_stop" },
	{ 0x71c90087, "memcmp" },
	{ 0x4ae9cef5, "wait_for_completion_interruptible" },
	{ 0xf0b2a9d, "free_netdev" },
	{ 0x49184d49, "wiphy_unregister" },
	{ 0x328a05f1, "strncpy" },
	{ 0x888ba6b5, "register_netdev" },
	{ 0xcd47db37, "sdio_readl" },
	{ 0xc212a112, "_raw_spin_unlock_irq" },
	{ 0x84b183ae, "strncmp" },
	{ 0xe0c07fd2, "brcmu_pktq_peek_tail" },
	{ 0xb3d7206e, "brcmu_pktq_flush" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x9488611d, "skb_push" },
	{ 0xc0f2bb5b, "brcmu_pktq_pdeq" },
	{ 0xd8b39e3a, "mutex_lock" },
	{ 0x704b6f4b, "cfg80211_connect_result" },
	{ 0x30386069, "cfg80211_michael_mic_failure" },
	{ 0xb5a0e267, "down" },
	{ 0x6ed4557e, "mod_timer" },
	{ 0x22d88e1d, "add_timer" },
	{ 0x1fccbd4e, "skb_pull" },
	{ 0xafb5aaa, "cfg80211_ibss_joined" },
	{ 0xd79b5a02, "allow_signal" },
	{ 0x5831f7c9, "sdio_readsb" },
	{ 0xbc7fdc56, "sdio_unregister_driver" },
	{ 0x1d0fb394, "sdio_f0_writeb" },
	{ 0x99c4cd6e, "sdio_set_host_pm_flags" },
	{ 0x42da3c2f, "skb_queue_tail" },
	{ 0x488fa28e, "cfg80211_inform_bss" },
	{ 0x7a7df7b2, "cfg80211_roamed" },
	{ 0xd7779230, "cfg80211_put_bss" },
	{ 0xd16ab534, "wiphy_new" },
	{ 0x12a38747, "usleep_range" },
	{ 0x179fd8d, "wiphy_register" },
	{ 0xb368ec89, "_raw_spin_unlock_bh" },
	{ 0xfdc391eb, "sdio_release_irq" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0xd59daefe, "_raw_spin_lock_irq" },
	{ 0x6fcc633d, "alloc_netdev_mqs" },
	{ 0x996bdb64, "_kstrtoul" },
	{ 0xf941ce04, "eth_type_trans" },
	{ 0xc27487dd, "__bug" },
	{ 0x3b0d0146, "sdio_f0_readb" },
	{ 0x4a376501, "wake_up_process" },
	{ 0x48c3a804, "ether_setup" },
	{ 0xcc5005fe, "msleep_interruptible" },
	{ 0x13155cab, "cfg80211_disconnected" },
	{ 0x94dea09a, "kmem_cache_alloc_trace" },
	{ 0xbd7083bc, "_raw_spin_lock_irqsave" },
	{ 0x72542c85, "__wake_up" },
	{ 0xf6ebc03b, "net_ratelimit" },
	{ 0xd2965f6f, "kthread_should_stop" },
	{ 0x41719638, "sdio_memcpy_toio" },
	{ 0x5c1e551e, "sdio_writew" },
	{ 0x5807024, "brcmu_pktq_init" },
	{ 0x3fdacc6f, "add_wait_queue" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x32f80ea9, "prepare_to_wait" },
	{ 0x7a728ef4, "up" },
	{ 0x1e309fa6, "request_firmware" },
	{ 0xf83178bd, "finish_wait" },
	{ 0x578a411d, "debugfs_create_u16" },
	{ 0xde6bdc1f, "unregister_netdev" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x4f7f2d1b, "complete" },
	{ 0x701d0ebd, "snprintf" },
	{ 0xbe0f009d, "__netif_schedule" },
	{ 0x580acc38, "sdio_readw" },
	{ 0x23613313, "sdio_register_driver" },
	{ 0x786d6e01, "consume_skb" },
	{ 0xcb343147, "sdio_memcpy_fromio" },
	{ 0x4411e126, "sdio_claim_host" },
	{ 0x8416c059, "cfg80211_scan_done" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0xa10eb05, "brcmu_pkt_buf_free_skb" },
	{ 0x38ab9e4d, "dev_get_drvdata" },
	{ 0x23b4b25c, "sdio_set_block_size" },
	{ 0xfa4e9d22, "release_firmware" },
	{ 0x6e720ff2, "rtnl_unlock" },
	{ 0x1896427b, "sdio_disable_func" },
	{ 0x341a8657, "sdio_release_host" },
	{ 0xe914e41e, "strcpy" },
	{ 0x5d4ba406, "brcmu_pktq_mlen" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=brcmutil";

MODULE_ALIAS("sdio:c*v02D0d4329*");
MODULE_ALIAS("sdio:c*v02D0d4330*");
