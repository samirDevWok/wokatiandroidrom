#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xd818474, "module_layout" },
	{ 0xe0de135d, "skb_queue_head" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x3922c8d, "skb_unlink" },
	{ 0xc9a76597, "skb_dequeue_tail" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x1862421c, "dev_alloc_skb" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x655ba6d8, "dev_kfree_skb_any" },
	{ 0x42da3c2f, "skb_queue_tail" },
	{ 0x5ad5434c, "skb_dequeue" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x786d6e01, "consume_skb" },
	{ 0x64a076c0, "skb_put" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

