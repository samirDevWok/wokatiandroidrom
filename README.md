# README #

### About
This is repository to keep changes which are made during customization of Android sources, that is during customizing Android ROM for Wokati COS

### Setup

First read https://github.com/rabeeh/android-imx6-kitkat and https://source.android.com/source/initializing

for setting up build environment before patching with changes from this repo

### Contact

samir@wokati.co
